<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 16/01/17
 * Time: 11:54
 */

require_once __DIR__.'/../vendor/autoload.php';

use MeaningCloud\API\MeaningCloud;

$topics = MeaningCloud::topics('<<<YOUR ACCESS KEY>>>')
    ->language('es')
    ->text('MeaningCloud es la mejor api NLP del mundo.')
    ->topicTypes('a')
    ->analyze();

print_r($topics);
