<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 16/01/17
 * Time: 11:08
 */

namespace MeaningCloud\Response;


class Status
{
    private $code;
    private $msg;
    private $credits;
    private $remainingCredits;

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @param string $msg
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;
    }

    /**
     * @return int
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * @param int $credits
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;
    }

    /**
     * @return int
     */
    public function getRemainingCredits()
    {
        return $this->remainingCredits;
    }

    /**
     * @param int $remainingCredits
     */
    public function setRemainingCredits($remainingCredits)
    {
        $this->remainingCredits = $remainingCredits;
    }
}
