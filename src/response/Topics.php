<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 16/01/17
 * Time: 11:47
 */

namespace MeaningCloud\Response;


class Topics
{
    private $status;
    private $entityList;
    private $conceptList;
    private $timeExpressionList;
    private $moneyExpressionList;
    private $quantityExpressionList;
    private $otherExpressionList;
    private $quotationList;
    private $relationList;

    /**
     * @return Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param Status $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \MeaningCloud\Domain\Topics\Entity[]
     */
    public function getEntityList()
    {
        return $this->entityList;
    }

    /**
     * @param \MeaningCloud\Domain\Topics\Entity[] $entityList
     */
    public function setEntityList($entityList)
    {
        $this->entityList = $entityList;
    }

    /**
     * @return \MeaningCloud\Domain\Topics\Concept[]
     */
    public function getConceptList()
    {
        return $this->conceptList;
    }

    /**
     * @param \MeaningCloud\Domain\Topics\Concept[] $conceptList
     */
    public function setConceptList($conceptList)
    {
        $this->conceptList = $conceptList;
    }

    /**
     * @return \MeaningCloud\Domain\Topics\TimeExpression[]
     */
    public function getTimeExpressionList()
    {
        return $this->timeExpressionList;
    }

    /**
     * @param \MeaningCloud\Domain\Topics\TimeExpression[] $timeExpressionList
     */
    public function setTimeExpressionList($timeExpressionList)
    {
        $this->timeExpressionList = $timeExpressionList;
    }

    /**
     * @return \MeaningCloud\Domain\Topics\MoneyExpression[]
     */
    public function getMoneyExpressionList()
    {
        return $this->moneyExpressionList;
    }

    /**
     * @param \MeaningCloud\Domain\Topics\MoneyExpression[] $moneyExpressionList
     */
    public function setMoneyExpressionList($moneyExpressionList)
    {
        $this->moneyExpressionList = $moneyExpressionList;
    }

    /**
     * @return \MeaningCloud\Domain\Topics\QuantityExpression[]
     */
    public function getQuantityExpressionList()
    {
        return $this->quantityExpressionList;
    }

    /**
     * @param \MeaningCloud\Domain\Topics\QuantityExpression[] $quantityExpressionList
     */
    public function setQuantityExpressionList($quantityExpressionList)
    {
        $this->quantityExpressionList = $quantityExpressionList;
    }

    /**
     * @return \MeaningCloud\Domain\Topics\OtherExpression[]
     */
    public function getOtherExpressionList()
    {
        return $this->otherExpressionList;
    }

    /**
     * @param \MeaningCloud\Domain\Topics\OtherExpression[] $otherExpressionList
     */
    public function setOtherExpressionList($otherExpressionList)
    {
        $this->otherExpressionList = $otherExpressionList;
    }

    /**
     * @return \MeaningCloud\Domain\Topics\Quotation[]
     */
    public function getQuotationList()
    {
        return $this->quotationList;
    }

    /**
     * @param \MeaningCloud\Domain\Topics\Quotation[] $quotationList
     */
    public function setQuotationList($quotationList)
    {
        $this->quotationList = $quotationList;
    }

    /**
     * @return \MeaningCloud\Domain\Topics\Relation[]
     */
    public function getRelationList()
    {
        return $this->relationList;
    }

    /**
     * @param \MeaningCloud\Domain\Topics\Relation[] $relationList
     */
    public function setRelationList($relationList)
    {
        $this->relationList = $relationList;
    }
}
