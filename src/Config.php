<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 10:55
 */

namespace MeaningCloud;


class Config
{
    private $config;

    /**
     * Config constructor.
     */
    public function __construct()
    {
        $this->config = include __DIR__.'/../config/config.php';
    }

    public function get($param, $default='')
    {
        if (isset($this->config[$param])) {
            return $this->config[$param];
        } else {
            return $default;
        }
    }
}
