<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 10:46
 */

namespace MeaningCloud\API\Template;

use MeaningCloud\Config;
use MeaningCloud\Request\Template\TopicsRequest;
use MeaningCloud\Response\Topics;
use Unirest\Request;
use Unirest\Request\Body;

class TopicsTemplate
{
    private $topicsEndpoint;
    private $request;
    private $key;

    /**
     * TopicsTemplate constructor.
     */
    public function __construct($key)
    {
        $config = new Config();
        $this->topicsEndpoint = $config->get('topics_endpoint');
        $url = $config->get('meaningcloud_url').$config->get('topics_endpoint');
        $this->request = new TopicsRequest($url);
        $this->key = $key;
    }

    public function outputFormat($of)
    {
        $this->request->outputFormat($of);
        return $this;
    }

    public function language($lang)
    {
        $this->request->language($lang);
        return $this;
    }

    public function interfaceLanguage($ilang)
    {
        $this->request->interfaceLanguage($ilang);
        return $this;
    }

    public function text($txt)
    {
        $this->request->text($txt);
        return $this;
    }

    public function textFormat($txtf)
    {
        $this->request->textFormat($txtf);
        return $this;
    }

    public function url($url)
    {
        $this->request->url($url);
        return $this;
    }

    public function document($doc)
    {
        $this->request->document($doc);
        return $this;
    }

    public function topicTypes($tt)
    {
        $this->request->topicTypes($tt);
        return $this;
    }

    public function unknownWords($uw)
    {
        $this->request->unknownWords($uw);
        return $this;
    }

    public function relaxedTypography($rt)
    {
        $this->request->relaxedTypography($rt);
        return $this;
    }

    public function userDictionary($ud)
    {
        $this->request->userDictionary($ud);
        return $this;
    }

    public function showSubtopics($st)
    {
        $this->request->showSubtopics($st);
        return $this;
    }

    public function analyze()
    {
        $headers = ['Accept' => 'application/json'];
        $this->request->licenseKey($this->key);
        $body = Body::Form($this->request->getParams());
        $response = Request::post($this->request->getUrl(), $headers, $body);
        $response_json = json_decode($response->raw_body);
        $mapper = new \JsonMapper();
        $topics = $mapper->map($response_json, new Topics());
        return $topics;
    }
}
