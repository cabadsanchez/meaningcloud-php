<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 10:40
 */

namespace MeaningCloud\API;

use MeaningCloud\Config;
use MeaningCloud\API\Template\TopicsTemplate;

class MeaningCloud
{
    public static function topics($key): TopicsTemplate
    {
        $config = new Config();
        return new TopicsTemplate($key);
    }
}
