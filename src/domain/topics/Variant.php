<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 13:47
 */

namespace MeaningCloud\Domain\Topics;


class Variant
{
    private $form;
    private $inip;
    private $endp;

    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param string $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return int
     */
    public function getInip()
    {
        return $this->inip;
    }

    /**
     * @param int $inip
     */
    public function setInip($inip)
    {
        $this->inip = $inip;
    }

    /**
     * @return int
     */
    public function getEndp()
    {
        return $this->endp;
    }

    /**
     * @param int $endp
     */
    public function setEndp($endp)
    {
        $this->endp = $endp;
    }
}
