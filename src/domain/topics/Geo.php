<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 13:29
 */

namespace MeaningCloud\Domain\Topics;


class Geo
{
    private $form;
    private $id;
    private $standardList;

    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param string $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Standard[]
     */
    public function getStandardList()
    {
        return $this->standardList;
    }

    /**
     * @param Standard[] $standardList
     */
    public function setStandardList($standardList)
    {
        $this->standardList = $standardList;
    }

}
