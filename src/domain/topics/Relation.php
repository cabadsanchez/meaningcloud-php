<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 14:49
 */

namespace MeaningCloud\Domain\Topics;


class Relation
{
    private $form;
    private $inip;
    private $endp;
    private $subject;
    private $verb;
    private $complementList;
    private $degree;

    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param string $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return int
     */
    public function getInip()
    {
        return $this->inip;
    }

    /**
     * @param int $inip
     */
    public function setInip($inip)
    {
        $this->inip = $inip;
    }

    /**
     * @return int
     */
    public function getEndp()
    {
        return $this->endp;
    }

    /**
     * @param int $endp
     */
    public function setEndp($endp)
    {
        $this->endp = $endp;
    }

    /**
     * @return Subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param Subject $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return Verb
     */
    public function getVerb()
    {
        return $this->verb;
    }

    /**
     * @param Verb $verb
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;
    }

    /**
     * @return Complement[]
     */
    public function getComplementList()
    {
        return $this->complementList;
    }

    /**
     * @param Complement[] $complementList
     */
    public function setComplementList($complementList)
    {
        $this->complementList = $complementList;
    }

    /**
     * @return double
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * @param double $degree
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;
    }
}
