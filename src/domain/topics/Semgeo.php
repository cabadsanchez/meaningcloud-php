<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 7/03/17
 * Time: 17:44
 */

namespace MeaningCloud\Domain\Topics;


class Semgeo
{
    private $continent;
    private $country;
    private $adm1;
    private $adm2;
    private $adm3;
    private $city;
    private $district;

    /**
     * @return Geo
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * @param Geo $continent
     */
    public function setContinent($continent)
    {
        $this->continent = $continent;
    }

    /**
     * @return Geo
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Geo $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return Geo
     */
    public function getAdm1()
    {
        return $this->adm1;
    }

    /**
     * @param Geo $adm1
     */
    public function setAdm1($adm1)
    {
        $this->adm1 = $adm1;
    }

    /**
     * @return Geo
     */
    public function getAdm2()
    {
        return $this->adm2;
    }

    /**
     * @param Geo $adm2
     */
    public function setAdm2($adm2)
    {
        $this->adm2 = $adm2;
    }

    /**
     * @return Geo
     */
    public function getAdm3()
    {
        return $this->adm3;
    }

    /**
     * @param Geo $adm3
     */
    public function setAdm3($adm3)
    {
        $this->adm3 = $adm3;
    }

    /**
     * @return Geo
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param Geo $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return Geo
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param Geo $district
     */
    public function setDistrict($district)
    {
        $this->district = $district;
    }
}
