<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 14:44
 */

namespace MeaningCloud\Domain\Topics;


class QuantityExpression
{
    private $form;
    private $amountForm;
    private $numericValue;
    private $unit;
    private $inip;
    private $endp;

    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param string $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return string
     */
    public function getAmountForm()
    {
        return $this->amountForm;
    }

    /**
     * @param string $amountForm
     */
    public function setAmountForm($amountForm)
    {
        $this->amountForm = $amountForm;
    }

    /**
     * @return string
     */
    public function getNumericValue()
    {
        return $this->numericValue;
    }

    /**
     * @param string $numericValue
     */
    public function setNumericValue($numericValue)
    {
        $this->numericValue = $numericValue;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return int
     */
    public function getInip()
    {
        return $this->inip;
    }

    /**
     * @param int $inip
     */
    public function setInip($inip)
    {
        $this->inip = $inip;
    }

    /**
     * @return int
     */
    public function getEndp()
    {
        return $this->endp;
    }

    /**
     * @param int $endp
     */
    public function setEndp($endp)
    {
        $this->endp = $endp;
    }
}
