<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 14:37
 */

namespace MeaningCloud\Domain\Topics;


class TimeExpression
{
    private $form;
    private $normalizedForm;
    private $actualTime;
    private $precision;
    private $inip;
    private $endp;

    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param string $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return string
     */
    public function getNormalizedForm()
    {
        return $this->normalizedForm;
    }

    /**
     * @param string $normalizedForm
     */
    public function setNormalizedForm($normalizedForm)
    {
        $this->normalizedForm = $normalizedForm;
    }

    /**
     * @return string
     */
    public function getActualTime()
    {
        return $this->actualTime;
    }

    /**
     * @param string $actualTime
     */
    public function setActualTime($actualTime)
    {
        $this->actualTime = $actualTime;
    }

    /**
     * @return string
     */
    public function getPrecision()
    {
        return $this->precision;
    }

    /**
     * @param string $precision
     */
    public function setPrecision($precision)
    {
        $this->precision = $precision;
    }

    /**
     * @return int
     */
    public function getInip()
    {
        return $this->inip;
    }

    /**
     * @param int $inip
     */
    public function setInip($inip)
    {
        $this->inip = $inip;
    }

    /**
     * @return int
     */
    public function getEndp()
    {
        return $this->endp;
    }

    /**
     * @param int $endp
     */
    public function setEndp($endp)
    {
        $this->endp = $endp;
    }
}
