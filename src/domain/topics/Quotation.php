<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 14:47
 */

namespace MeaningCloud\Domain\Topics;


class Quotation
{
    private $form;
    private $who;
    private $verb;
    private $inip;
    private $endp;

    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param string $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return string
     */
    public function getWho()
    {
        return $this->who;
    }

    /**
     * @param string $who
     */
    public function setWho($who)
    {
        $this->who = $who;
    }

    /**
     * @return string
     */
    public function getVerb()
    {
        return $this->verb;
    }

    /**
     * @param string $verb
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;
    }

    /**
     * @return int
     */
    public function getInip()
    {
        return $this->inip;
    }

    /**
     * @param int $inip
     */
    public function setInip($inip)
    {
        $this->inip = $inip;
    }

    /**
     * @return int
     */
    public function getEndp()
    {
        return $this->endp;
    }

    /**
     * @param int $endp
     */
    public function setEndp($endp)
    {
        $this->endp = $endp;
    }
}
