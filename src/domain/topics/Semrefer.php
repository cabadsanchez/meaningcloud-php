<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 13:36
 */

namespace MeaningCloud\Domain\Topics;


class Semrefer
{
    private $organization;
    private $affinity;

    /**
     * @return Refer
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Refer $organization
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;
    }

    /**
     * @return Refer
     */
    public function getAffinity()
    {
        return $this->affinity;
    }

    /**
     * @param Refer $affinity
     */
    public function setAffinity($affinity)
    {
        $this->affinity = $affinity;
    }
}
