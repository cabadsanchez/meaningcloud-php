<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 13:25
 */

namespace MeaningCloud\Domain\Topics;


class Concept
{
    private $form;
    private $officialForm;
    private $dictionary;
    private $id;
    private $sementity;
    private $semgeoList;
    private $semldList;
    private $semthemeList;
    private $standardList;
    private $variantList;
    private $relevance;
    private $subentityList;

    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param string $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return string
     */
    public function getOfficialForm()
    {
        return $this->officialForm;
    }

    /**
     * @param string $officialForm
     */
    public function setOfficialForm($officialForm)
    {
        $this->officialForm = $officialForm;
    }

    /**
     * @return string
     */
    public function getDictionary()
    {
        return $this->dictionary;
    }

    /**
     * @param string $dictionary
     */
    public function setDictionary($dictionary)
    {
        $this->dictionary = $dictionary;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Sementity
     */
    public function getSementity()
    {
        return $this->sementity;
    }

    /**
     * @param Sementity $sementity
     */
    public function setSementity($sementity)
    {
        $this->sementity = $sementity;
    }

    /**
     * @return Semgeo[]
     */
    public function getSemgeoList()
    {
        return $this->semgeoList;
    }

    /**
     * @param Semgeo[] $semgeoList
     */
    public function setSemgeoList($semgeoList)
    {
        $this->semgeoList = $semgeoList;
    }

    /**
     * @return string[]
     */
    public function getSemldList()
    {
        return $this->semldList;
    }

    /**
     * @param string[] $semldList
     */
    public function setSemldList($semldList)
    {
        $this->semldList = $semldList;
    }

    /**
     * @return Semtheme[]
     */
    public function getSemthemeList()
    {
        return $this->semthemeList;
    }

    /**
     * @param Semtheme[] $semthemeList
     */
    public function setSemthemeList($semthemeList)
    {
        $this->semthemeList = $semthemeList;
    }

    /**
     * @return Standard[]
     */
    public function getStandardList()
    {
        return $this->standardList;
    }

    /**
     * @param Standard[] $standardList
     */
    public function setStandardList($standardList)
    {
        $this->standardList = $standardList;
    }

    /**
     * @return Variant[]
     */
    public function getVariantList()
    {
        return $this->variantList;
    }

    /**
     * @param Variant[] $variantList
     */
    public function setVariantList($variantList)
    {
        $this->variantList = $variantList;
    }

    /**
     * @return double
     */
    public function getRelevance()
    {
        return $this->relevance;
    }

    /**
     * @param double $relevance
     */
    public function setRelevance($relevance)
    {
        $this->relevance = $relevance;
    }

    /**
     * @return Entity[]
     */
    public function getSubentityList()
    {
        return $this->subentityList;
    }

    /**
     * @param Entity[] $subentityList
     */
    public function setSubentityList($subentityList)
    {
        $this->subentityList = $subentityList;
    }

}
