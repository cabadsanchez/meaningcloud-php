<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 16/01/17
 * Time: 10:56
 */

namespace MeaningCloud\Domain\Topics;


class Complement
{
    private $form;
    private $type;

    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param string $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}
