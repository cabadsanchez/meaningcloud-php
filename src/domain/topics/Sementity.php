<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 13:26
 */

namespace MeaningCloud\Domain\Topics;


class Sementity
{
    private $class;
    private $fiction;
    private $id;
    private $type;
    private $confidence;

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getFiction()
    {
        return $this->fiction;
    }

    /**
     * @param string $fiction
     */
    public function setFiction($fiction)
    {
        $this->fiction = $fiction;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getConfidence()
    {
        return $this->confidence;
    }

    /**
     * @param string $confidence
     */
    public function setConfidence($confidence)
    {
        $this->confidence = $confidence;
    }

}
