<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 16/01/17
 * Time: 10:52
 */

namespace MeaningCloud\Domain\Topics;


class Subject
{
    private $form;
    private $lemmaList;
    private $senseIdList;

    /**
     * @return string
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param string $form
     */
    public function setForm($form)
    {
        $this->form = $form;
    }

    /**
     * @return string[]
     */
    public function getLemmaList()
    {
        return $this->lemmaList;
    }

    /**
     * @param string[] $lemmaList
     */
    public function setLemmaList($lemmaList)
    {
        $this->lemmaList = $lemmaList;
    }

    /**
     * @return string[]
     */
    public function getSenseIdList()
    {
        return $this->senseIdList;
    }

    /**
     * @param string[] $senseIdList
     */
    public function setSenseIdList($senseIdList)
    {
        $this->senseIdList = $senseIdList;
    }
}
