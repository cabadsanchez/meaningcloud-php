<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 11:17
 */

namespace MeaningCloud\Request;

class MeaningCloudRequest
{
    private $url;
    protected $params;

    /**
     * MeaningCloudRequest constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $this->url = $url;
        $this->params = [];
        $this->params['src'] = 'meaningcloud-php';
    }

    public function licenseKey($key) {
        $this->params['key'] = $key;
        return $this;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }
}
