<?php
/**
 * Created by PhpStorm.
 * User: cabad
 * Date: 13/01/17
 * Time: 12:08
 */

namespace MeaningCloud\Request\Template;

use MeaningCloud\Request\MeaningCloudRequest;

class TopicsRequest extends MeaningCloudRequest
{

    public function outputFormat($of)
    {
        $this->params['of'] = $of;
        return $this;
    }

    public function language($lang)
    {
        $this->params['lang'] = $lang;
        return $this;
    }

    public function interfaceLanguage($ilang)
    {
        $this->params['ilang'] = $ilang;
        return $this;
    }

    public function text($txt)
    {
        $this->params['txt'] = $txt;
        return $this;
    }

    public function textFormat($txtf)
    {
        $this->params['txtf'] = $txtf;
        return $this;
    }

    public function url($url)
    {
        $this->params['url'] = $url;
        return $this;
    }

    public function document($doc)
    {
        $this->params['doc'] = $doc;
        return $this;
    }

    public function topicTypes($tt)
    {
        $this->params['tt'] = $tt;
        return $this;
    }

    public function unknownWords($uw)
    {
        $this->params['uw'] = $uw;
        return $this;
    }

    public function relaxedTypography($rt)
    {
        $this->params['rt'] = $rt;
        return $this;
    }

    public function userDictionary($ud)
    {
        $this->params['ud'] = $ud;
        return $this;
    }

    public function showSubtopics($st)
    {
        $this->params['st'] = $st;
        return $this;
    }
}
